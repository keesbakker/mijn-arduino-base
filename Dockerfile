# This is a docker to be the base of other Arduino dockers.

FROM ubuntu:18.04

LABEL maintainer="Kees Bakker <kees.bakker@sodaq.com>"

RUN apt-get update -qq && \
    apt-get -qq -y install \
        wget \
        curl \
        git \
        xz-utils

ENV ARDUINO_VERSION 1.8.6

RUN wget -nv http://downloads.arduino.cc/arduino-${ARDUINO_VERSION}-linux64.tar.xz \
    && tar xf arduino-${ARDUINO_VERSION}-linux64.tar.xz -C /usr/local/share \
    && ln -s /usr/local/share/arduino-${ARDUINO_VERSION} /usr/local/share/arduino \
    && ln -s /usr/local/share/arduino-${ARDUINO_VERSION}/arduino /usr/local/bin/arduino

RUN arduino --install-boards "arduino:samd"

# Cleanup
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/*

CMD ["/bin/bash"]
